<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>KIJ</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
</head>
<body>
    <div class="container">
        <div class="jumbotron">
            <h1 class="display-4 text-center">APLIKASI KRIPTOGRAFI</h1>
            <p class="lead text-center">Akmal Fikri, Ammar Alvi H, Choirul Arif M, Krismon, Rama Ulgasesa</p>
            <hr class="my-4">
            <div class="row">
                <div class="col">
                    <hr class="my-4">
                    <form action="#" type="post">
                        <div class="form-group" id="kunci">
                            <label for="plain">Key</label>
                            <input class="form-control"  id="key" name="key" rows="3">
                        </div>
                        <div class="form-group">
                            <label for="plain">Plain Text</label>
                            <input class="form-control"  disabled id="plain" name="plain" rows="3">
                        </div>
                        <div class="form-group">
                            <label for="typeEnkripsi">Type Enkripsi</label>
                            <select name="typeEnkripsi" id="typeEnkripsi" class="form-control">
                                <option value="">-- Pilih Salah Satu --</option>
                                <option value="caesar">Caesar Cipher</option>
                                <option value="rot13">Rot 13</option>
                                <option value="vigenere">Vigenere</option>
                                <option value="md5">md5</option>
                            </select>
                        </div>
                    </form>
                </div>
                <div class="col">
                    <hr class="my-4">
                    <form action="#" type="post">
                        <div class="form-group"class="text-center">
                            <label for="chipher">Cipher Text</label>
                            <input class="form-control" id="chipher" disabled name="chipher" rows="3">
                        </div>
                        <div class="btn btn-block btn-info"  style="display: none" id="btnDekrip">Dekrip</div>
                    </form>
                    <p id="type"></p>
                </div>
            </div>
            <hr>
        </div>
    </div>
    <script src="js/jquery-3.1.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script>
        $(window).ready(function() {
            var plain = $('#plain');
            var chipher = $('#chipher');
            var type = $('#typeEnkripsi');
            var key = null;
            $('#kunci').css('display','none');
            type.on('change',function() {
                if( type.val() != '' ){
                    $('#type').html('Type Enkripsi: '+ $(this).val());
                    plain.removeAttr('disabled');
                    chipher.removeAttr('disabled');
                    if($(this).val() == 'vigenere'){
                        $('#kunci').css('display','block');
                        plain.val(null);
                        chipher.val(null);
                    }else{
                        $('#kunci').css('display','none');
                    }
                    if($(this).val() == 'md5'){
                        chipher.attr('disabled','disabled');
                    }
                    if($(this).val() != 'vigenere'){
                        if(plain.val() != ''){
                            $.ajax({
                                url: 'function.php',
                                type: 'POST',
                                data: {
                                    type: type.val(),
                                    input: plain.val(),
                                    key: key,
                                    typeText: "plain"
                                },
                                success:function(hasil) {
                                    chipher.val(hasil)
                                }
                            })
                        }
                    }
                }else{
                    plain.attr('disabled','disabled');
                    chipher.attr('disabled','disabled');
                    $('#type').html('');
                    plain.val(null);
                    chipher.val(null);
                    $('#kunci').css('display','none');
                }
            });
            $('form input').keyup(function () {
                var typeText = $(this).attr('id');
                if(typeText == 'plain'){
                    input = $(this).val();
                    if(type.val() == 'vigenere'){
                        key = $('#key').val();
                    }
                }else if(typeText == 'chipher'){
                    input = $(this).val();
                    if(type.val() == 'vigenere'){
                        key = $('#key').val();
                    }
                }
                if(input != ''){
                    $.ajax({
                        url: 'function.php',
                        type: 'POST',
                        data: {
                            type: type.val(),
                            input: input,
                            key: key,
                            typeText: typeText
                        },
                        success:function(hasil) {
                            if(typeText == 'plain'){
                                chipher.val(hasil)
                            }else if(typeText == 'chipher'){
                                plain.val(hasil);
                            }
                        }
                    })
                }
            })
        })
    </script>
</body>
</html>