<?php
// caesar cipher
function Cipher($ch,$key)
{
	// if (!ctype_alpha($ch))
	// 	return $ch;

	$offset = ord(ctype_upper($ch) ? 'A' : 'a');
	// return chr($offset+$key);
	// return chr(fmod(((ord($ch) + $key) - $offset), 26) + $offset);
	return chr(ord($ch)+$key);
}
function enkripsiCipher($input, $key)
{
	$output = "";
	$inputArr = str_split($input);
	foreach ($inputArr as $ch)
		$output .= Cipher($ch, $key);

	return $output;
}
function dekripsiCipher($input, $key)
{
	return enkripsiCipher($input,-$key);
}
// rot 13
function rot13($val,$length)
{
	$char = '';
	$split = str_split($val);
	$i = 0;
	while ($i < strlen($val)) {
		$chiper_text = ord($split[$i]);
		if($chiper_text >= 97 && $chiper_text <= 109 || $chiper_text >= 65 && $chiper_text <= 77){
			if($length > 1){
				$char .= chr($chiper_text+$length);
			}else{
				$char .= chr($chiper_text-$length);
			}
		}else if($chiper_text >= 110 && $chiper_text <= 122 || $chiper_text >= 78 && $chiper_text <= 90){
			if($length > 1){
				$char .= chr($chiper_text-$length);
			}else{
				$char .= chr($chiper_text+$length);
			}
		}else if(!ctype_alpha($val)){
			$char .= $split[$i];
		}
		$i++;
	}
	return $char;
}
// vigenere
function mod($a,$b)
{
	return ($a % $b + $b) % $b;
}
function vigenere($input,$key,$encipher)
{
	$keyLen = strlen($key);
	// jika kata kunci != alphabet
	for ($i=0; $i < $keyLen ; $i++) { 
		if(!ctype_alpha($key))
		{
			return "";
		}
	}
	$output = "";
	$nonAlphaCharCount = 0;
	$inputLen = strlen($input);
	for ($i=0; $i < $inputLen ; $i++) { 
		if(ctype_alpha($input[$i])){
			$upper = ctype_upper($input[$i]);
			$offset = ord($upper ? "A" : "a");
			$keyIndex = ($i - $nonAlphaCharCount) % $keyLen;
			$k = ord($upper ? strtoupper($key[$keyIndex]) : strtolower($key[$keyIndex]))-$offset;
			$k = $encipher ? $k : -$k;
			$ch = chr((Mod(((ord($input[$i]) + $k) - $offset), 26)) + $offset);
			$output .= $ch;
		}else{
			$output .= $input[$i];
			++$nonAlphaCharCount;
		}
	}
	return $output;
}
function vigenereEncrypt($input, $key)
{
	return vigenere($input, $key, true);
}

function vigenereDecrypt($input, $key)
{
	return vigenere($input, $key, false);
}

$type = $_POST['type'];
$data = $_POST['input'];
$typeText = $_POST['typeText'];
$offset = 3;
$key = $_POST['key'];
if($type == 'caesar' && $typeText == 'plain'){
   	echo enkripsiCipher($data,$offset);
}else if($type == 'caesar' && $typeText == 'chipher'){
	echo dekripsiCipher($data,$offset);
}elseif($type == 'rot13' && $typeText == 'plain'){
	// echo rot13($data,13);
	echo rot13($data,13);
}elseif($type == 'rot13' && $typeText == 'chipher'){
	echo rot13($data,-13);
}else if($type == 'vigenere' && $typeText == 'plain'){
	if($key != null){
		echo vigenereEncrypt($data,$key);
	}
}else if($type == 'vigenere' && $typeText == 'chipher'){
	if($key != null){
		echo vigenereDecrypt($data,$key);
	}
}else{
	echo md5($data);
}
?>